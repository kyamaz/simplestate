import { AuthService } from "@appServices/auth.service";
import { Injectable, PLATFORM_ID, Inject } from "@angular/core";
import { CanDeactivate, CanActivate, Router, ActivatedRoute } from "@angular/router";
import { WindowRef } from "src/app/win_ref";
import { HttpClient } from "@angular/common/http";
import { Observable, of, ReplaySubject } from "rxjs";
import { isPlatformBrowser } from "@angular/common";
import { tap, filter, map, switchMap, first } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class RoutesGuardService implements CanActivate {
  private authorized = {
    session_token: null
  };
  private has_auth$: ReplaySubject<boolean> = new ReplaySubject(1);
  private has_logged: boolean = false;

  constructor(
    private _router: Router,
    private winRef: WindowRef,
    private _authService: AuthService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  canActivate(): Observable<boolean> {
    //angular universal

    return of(true);

    /* if (isPlatformBrowser(this.platformId)) {
      if (window !== undefined) {
        this.has_logged = !!this.winRef.nativeWindow.localStorage.getItem(
          "session_token"
        );
        if (this.has_logged) {
          return this._authService.checkAuthentication().pipe(
            map(response => {
              if (response) {
                return true;
              }
              this._router.navigate(["login"]);
              return false;
            })
          );
        }
      }
      this._router.navigate(["login"]);
      return of(false);
    } */
  }
}
export class LeaveAppGuard implements CanDeactivate<any> {
  constructor() {}
  canDeactivate(component: any) {
    return component.logOut();
  }
}
