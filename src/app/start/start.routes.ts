import { StartContainerComponent } from "./container/start-container/start-container.component";
import { Routes } from "@angular/router";

export const ROUTES: Routes = [
  {
    path: "",
    component: StartContainerComponent
  },
  { path: "**", redirectTo: "./../404" }
];
