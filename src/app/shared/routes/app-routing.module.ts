import { LoginComponent } from "./../../login/login/login.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RoutesGuardService } from "./routes.guard.service";
const routes: Routes = [
  { path: "login", component: LoginComponent },
  {
    path: "app",
    canActivate: [RoutesGuardService],
    loadChildren: "./../../start/start.module#StartModule"
  },
  { path: "**", redirectTo: "/app", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
