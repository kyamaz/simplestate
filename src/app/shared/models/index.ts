import { FormGroup } from "@angular/forms";
export interface viewModelWithFormGroup {
  data: FormGroup;
}
export interface formArrayAction {
  type: "push" | "remove";
  id: number;
}
