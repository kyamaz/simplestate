import { ROUTES } from "./start.routes";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { StartContainerComponent } from "./container/start-container/start-container.component";
import { TabsComponent } from "./components/tabs/tabs.component";
import { HeadWidgetComponent } from "./components/head-widget/head-widget.component";
import { TabFormComponent } from "./components/tab-form/tab-form.component";
import { SharedModule } from "../shared/modules";

@NgModule({
  declarations: [
    StartContainerComponent,
    TabsComponent,
    HeadWidgetComponent,
    TabFormComponent
  ],
  imports: [SharedModule, RouterModule.forChild(ROUTES)]
})
export class StartModule {}
