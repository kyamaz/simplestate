import {
  MatInputModule,
  MatButtonModule,
  MatDialogModule,
  MatSelectModule,
  MatTabsModule,
  MatAutocompleteModule,
  ErrorStateMatcher
} from "@angular/material";
import { NgModule, ErrorHandler } from "@angular/core";
import { CommonModule } from "@angular/common";
//form
import { ReactiveFormsModule } from "@angular/forms";
@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatTabsModule,
    MatAutocompleteModule,
    ReactiveFormsModule
  ],

  exports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatTabsModule,
    MatAutocompleteModule,
    ReactiveFormsModule
  ],
  providers: [
    // { provide: ErrorStateMatcher, useClass: FormError },
  ]
})
export class SharedModule {}
