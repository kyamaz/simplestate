import * as _cloneDeep from "lodash/fp/cloneDeep";
export { _cloneDeep };

export function safeGet<T extends any>(value: Object, ...path: string[]): T {
  return path.reduce((prev: Object, prop: string) => {
    if (prev && prev.hasOwnProperty(prop)) {
      return prev[prop];
    } else {
      return null;
    }
  }, value);
}

export function isObject(obj): boolean {
  return obj === Object(obj);
}
