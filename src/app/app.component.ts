import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { WindowRef } from "./win_ref";
import { fadeInAnimation } from "./shared/animations/animations";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  animations: [fadeInAnimation]
})
export class AppComponent implements OnInit {
  public server$ = new Subject();
  constructor(
    private _router: Router,
    private winRef: WindowRef,
    //  private _notifyResponseService: NotifyResponseService,
    private route: ActivatedRoute
  ) {}
  ngOnInit() {
    this.subServerResp();
  }
  subServerResp(): any {
    /*    return this._notifyResponseService.server_response$.subscribe(r =>
      this.server$.next(r)
    ); */
  }
  dismiss_notif(): void {
    // return this.server$.next({});
  }
  getRouteAnimation(outlet): any {
    return outlet.activatedRouteData.animation;
  }
}
