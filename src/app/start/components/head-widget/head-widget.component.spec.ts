import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadWidgetComponent } from './head-widget.component';

describe('HeadWidgetComponent', () => {
  let component: HeadWidgetComponent;
  let fixture: ComponentFixture<HeadWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
