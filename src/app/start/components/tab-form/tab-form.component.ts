import { Component, OnInit } from "@angular/core";
import { FormsService } from "@appServices/forms.service";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "app-tab-form",
  templateUrl: "./tab-form.component.html",
  styleUrls: ["./tab-form.component.scss"]
})
export class TabFormComponent implements OnInit {
  form: FormGroup;
  constructor(public _formService: FormsService<any>) {}

  ngOnInit() {
    this.form = this._formService.build_form({
      userName: null,
      firstName: null,
      lastName: null,
      score: 0
    });
  }
}
